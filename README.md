This is a Basic Application to display list of New York Schools.

Architecture:
 
MVVM

Libraries:

- Jetpack Paging
- Jetpack Navigation
- View Models
- Live Data
- KOIN for DI
- Kotlin Coroutines
- Facebook Shimmer

Preview:

https://drive.google.com/open?id=1TVAo-sQ9gc2YAYxoZKrelHU8wfVknaln